﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using HCM.Common.Models;
using HCM.WebApp.Module.Database;

namespace HCM.WebApp.WebApi.Services
{
    public class BUServices : IBUServices
    {
        readonly UnitOfWork unitOfWork;
        public BUServices(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public bool Delete(int key)
        {
            var businessUnit = unitOfWork.GetObjectByKey<BusinessUnit>(key);
            businessUnit.Delete();
            unitOfWork.CommitChanges();

            var deletedBU = unitOfWork.GetObjectByKey<BusinessUnit>(key);
            if (deletedBU == null)
            {
                return true;
            }
            return false;

        }

        public BUModel Get(string BUCode)
        {
            var BUDetails = unitOfWork.Query<BusinessUnit>().Where(x => x.Code == BUCode).Select(x => new BUModel
            {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code,
                AddressLine1 = x.AddressLine1,
                AddressLine2 = x.AddressLine2,
                IsActive = x.IsActive

            }).ToList();
            if (BUDetails == null)
            {
                return null;
            }
            return BUDetails.FirstOrDefault();
        }

        public List<BUModel> GetList()
        {
            return unitOfWork.Query<BusinessUnit>().Select(x => new BUModel
            {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code,
                AddressLine1 = x.AddressLine1,
                AddressLine2 = x.AddressLine2,
                IsActive = x.IsActive,

            }).ToList();
        }

        public BUModel Post(BUModel model)
        {
            BusinessUnit bu = new BusinessUnit(unitOfWork)
            {
                Name = model.Name,
                Code = model.Code,
                AddressLine1 = model.AddressLine1,
                AddressLine2 = model.AddressLine2,
                IsActive = model.IsActive
            };
            unitOfWork.CommitChanges();
            return new BUModel
            {
                Id = bu.Id,
                Name = bu.Name,
                Code = bu.Code,
                AddressLine1 = bu.AddressLine1,
                AddressLine2 = bu.AddressLine2,
                IsActive = bu.IsActive
            };
        }

        public bool Update(BUModel BUDetails)
        {
            try
            {

                var businessUnit = unitOfWork.GetObjectByKey<BusinessUnit>(BUDetails.Id);
                businessUnit.Id = BUDetails.Id;
                businessUnit.Name = BUDetails.Name;
                businessUnit.Code = BUDetails.Code;
                businessUnit.AddressLine1 = BUDetails.AddressLine1;
                businessUnit.AddressLine2 = BUDetails.AddressLine2;
                businessUnit.IsActive = BUDetails.IsActive;
                unitOfWork.CommitChanges();
                return true;
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessUnitServive - Update" + ex.InnerException.Message);
                return false;
                throw;
            }
        }

    }
}
