﻿using DevExpress.ExpressApp.Templates;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using DevExpress.XtraRichEdit.Model;
using HCM.Common.Models;
using HCM.WebApp.Module.Database;
using System.ComponentModel.Design;
using System.Xml.Linq;


namespace HCM.WebApp.WebApi.Services
{
    public class CompanyServices : ICompanyServices
    {
        readonly UnitOfWork unitOfWork;
        public CompanyServices(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public bool Delete(int key)
        {
            var company = unitOfWork.GetObjectByKey<Company>(key);
            company.Delete();
            unitOfWork.CommitChanges();

            var deletedCompany = unitOfWork.GetObjectByKey<Company>(key);
            if (deletedCompany == null)
            {
                return true;
            }
            return false;

        }

        public CompanyModel Get(string companyCode)
        {
            var comanyDetails = unitOfWork.Query<Company>().Where(x => x.Code == companyCode).Select(x => new CompanyModel
            {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code,
                AddressLine1 = x.AddressLine1,
                AddressLine2 = x.AddressLine2,
                City = x.City,
                State = x.State,
                Country = x.Country,
                CompanyLogo = x.CompanyLogo,
                PrimaryCurrency = x.PrimaryCurrency,
                SiteURL = x.SiteURL,
                SubDomain = x.SubDomain,
                IsActive = x.IsActive

            }).ToList();
            if (comanyDetails == null)
            {
                return null;
            }
            return comanyDetails.FirstOrDefault();
        }

        public List<CompanyModel> GetList()
        {
            return unitOfWork.Query<Company>().Select(x => new CompanyModel
            {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code,
                AddressLine1 = x.AddressLine1,
                AddressLine2 = x.AddressLine2,
                City = x.City,
                State = x.State,
                Country = x.Country,
                CompanyLogo = x.CompanyLogo,
                PrimaryCurrency = x.PrimaryCurrency,
                SiteURL = x.SiteURL,
                SubDomain = x.SubDomain,
                IsActive = x.IsActive
            }).ToList();
        }

        public CompanyModel Post(CompanyModel model)
        {
            Company company = new Company(unitOfWork)
            {
                Name = model.Name,
                Code = model.Code,
                AddressLine1 = model.AddressLine1,
                AddressLine2 = model.AddressLine2,
                City = model.City,
                State = model.State,
                Country = model.Country,
                CompanyLogo = model.CompanyLogo,
                PrimaryCurrency = model.PrimaryCurrency,
                SiteURL = model.SiteURL,
                SubDomain = model.SubDomain,
                IsActive = model.IsActive
            };
            unitOfWork.CommitChanges();
            return new CompanyModel
            {
                Id = company.Id,
                Name = company.Name,
                Code = company.Code,
                AddressLine1 = company.AddressLine1,
                AddressLine2 = company.AddressLine2,
                City = company.City,
                State = company.State,
                Country = company.Country,
                CompanyLogo = company.CompanyLogo,
                PrimaryCurrency = company.PrimaryCurrency,
                SiteURL = company.SiteURL,
                SubDomain = company.SubDomain,
                IsActive = company.IsActive
            };
        }

        public bool Update(CompanyModel companyDetails)
        {
            try
            {

                var company = unitOfWork.GetObjectByKey<Company>(companyDetails.Id);
                company.Id = companyDetails.Id;
                company.Name = companyDetails.Name;
                company.Code = companyDetails.Code;
                company.AddressLine1 = companyDetails.AddressLine1;
                company.AddressLine2 = companyDetails.AddressLine2;
                company.City = companyDetails.City;
                company.State = companyDetails.State;
                company.Country = companyDetails.Country;
                company.CompanyLogo = companyDetails.CompanyLogo;
                company.PrimaryCurrency = companyDetails.PrimaryCurrency;
                company.SiteURL = companyDetails.SiteURL;
                company.SubDomain = companyDetails.SubDomain;
                company.IsActive = companyDetails.IsActive;

                unitOfWork.CommitChanges();
                return true;
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("ComapnyServive - Update" + ex.InnerException.Message);
                return false;
                throw;
            }
        }
    }
}
