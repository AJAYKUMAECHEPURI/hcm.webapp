﻿using HCM.Common.Models;
using HCM.WebApp.Module.Database;

namespace HCM.WebApp.WebApi.Services
{
    public interface ICompanyServices
    {
        List<CompanyModel> GetList();
        CompanyModel Get(string companyCode);
        CompanyModel Post(CompanyModel model);
        bool Update(CompanyModel companyDetails);
        bool Delete(int key);
    }
}
