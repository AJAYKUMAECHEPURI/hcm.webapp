﻿using HCM.Common.Models;
using HCM.WebApp.Module.Database;

namespace HCM.WebApp.WebApi.Services
{
    public interface IBUServices
    {
        List<BUModel> GetList();

        BUModel Get(string companyCode);

        BUModel Post(BUModel model);
        bool Update(BUModel companyDetails);

        bool Delete(int key);
    }
}
