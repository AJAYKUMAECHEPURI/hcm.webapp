﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using HCM.Common.Models;
using HCM.WebApp.Module.Database;

namespace HCM.WebApp.WebApi.Services
{
    public class DepartmentServices : IDepartmentServices
    {
        readonly UnitOfWork unitOfWork;
        public DepartmentServices(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
        public bool Delete(int key)
        {
            var department = unitOfWork.GetObjectByKey<Department>(key);
            department.Delete();
            unitOfWork.CommitChanges();

            var deletedDepartment = unitOfWork.GetObjectByKey<Department>(key);
            if (deletedDepartment == null)
            {
                return true;
            }
            return false;
        }

        public DepartmentModel Get(string departmentCode)
        {
            var departmentDetails = unitOfWork.Query<Department>().Where(x => x.Code == departmentCode).Select(x => new DepartmentModel
            {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code,
                Email = x.Email,
                Description = x.Description,
                Director = x.Director,
                IsActive = x.IsActive

            }).ToList();
            if (departmentDetails == null)
            {
                return null;
            }
            return departmentDetails.FirstOrDefault();
        }

        public List<DepartmentModel> GetList()
        {
            return unitOfWork.Query<Department>().Select(x => new DepartmentModel
            {
                Id = x.Id,
                Name = x.Name,
                Code = x.Code,
                Email = x.Email,
                Description = x.Description,
                Director = x.Director,
                IsActive = x.IsActive

            }).ToList();
        }

        public DepartmentModel Post(DepartmentModel model)
        {
            Department dept = new Department(unitOfWork)
            {
                BUId = model.BUId,
                Name = model.Name,
                Code = model.Code,
                Email = model.Email,
                Description = model.Description,
                Director = model.Director,
                IsActive = model.IsActive
            };
            unitOfWork.CommitChanges();
            return new DepartmentModel
            {
                Id = dept.Id,
                Name = dept.Name,
                Code = dept.Code,
                Email = dept.Email,
                Description = dept.Description,
                Director = dept.Director,
                IsActive = dept.IsActive
            };
        }

        public bool Update(DepartmentModel departmentDetails)
        {
            try
            {

                var department = unitOfWork.GetObjectByKey<Department>(departmentDetails.Id);
                department.Id = departmentDetails.Id;
                department.Name = departmentDetails.Name;
                department.Code = departmentDetails.Code;
                department.Email = departmentDetails.Email;
                department.Description = departmentDetails.Description;
                department.Director = departmentDetails.Director;
                department.IsActive = departmentDetails.IsActive;
                unitOfWork.CommitChanges();
                return true;
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Department Servive - Update" + ex.InnerException.Message);
                return false;
                throw;
            }
        }
    }
}
