﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using DevExpress.XtraRichEdit.Model;
using HCM.Common.Models;
using HCM.WebApp.Module.Database;

namespace HCM.WebApp.WebApi.Services
{
    public class DesignationServices : IDesignationServices
    {
        readonly UnitOfWork unitOfWork;
        public DesignationServices(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public bool Delete(int key)
        {
            var designation = unitOfWork.GetObjectByKey<Designation>(key);
            designation.Delete();
            unitOfWork.CommitChanges();

            var deletedDesignation = unitOfWork.GetObjectByKey<Designation>(key);
            if (deletedDesignation == null)
            {
                return true;
            }
            return false;
        }

        public DesignationModel Get(string designationCode)
        {
            var DesignationDetails = unitOfWork.Query<Designation>().Where(x => x.Code == designationCode).Select(x => new DesignationModel
            {
                Id = x.Id,
                DepartmentId = x.DepartmentId,
                Code = x.Code,
                Description = x.Description,
                IsActive = x.IsActive

            }).ToList();
            if (DesignationDetails == null)
            {
                return null;
            }
            return DesignationDetails.FirstOrDefault();
        }

        public List<DesignationModel> GetList()
        {

            return unitOfWork.Query<Designation>().Select(x => new DesignationModel
            {
                Id = x.Id,
                DepartmentId = x.DepartmentId,
                Code = x.Code,
                Description = x.Description,
                IsActive = x.IsActive

            }).ToList();
        }

        public DesignationModel Post(DesignationModel model)
        {
            Designation desig = new Designation(unitOfWork)
            {
               
                Code = model.Code,
                DepartmentId= model.DepartmentId,
                Description = model.Description,
                IsActive = model.IsActive
            };
            unitOfWork.CommitChanges();
            return new DesignationModel
            {
                Id = desig.Id,
                Code = desig.Code,
                DepartmentId = desig.DepartmentId,
                Description = desig.Description,
                IsActive = desig.IsActive
            };
        }

        public bool Update(DesignationModel designationDetails)
        {
            try
            {
                var designation = unitOfWork.GetObjectByKey<Designation>(designationDetails.Id);
                designation.Id = designationDetails.Id;
                designation.DepartmentId = designationDetails.DepartmentId;
                designation.Code = designationDetails.Code;
                designation.Description = designationDetails.Description;
                designation.IsActive = designationDetails.IsActive;
                unitOfWork.CommitChanges();
                return true;
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Designation Servive - Update" + ex.InnerException.Message);
                return false;
                throw;
            }
        }
    }
}
