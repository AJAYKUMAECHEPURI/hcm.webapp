﻿using HCM.Common.Models;

namespace HCM.WebApp.WebApi.Services
{
    public interface IDesignationServices
    {
        List<DesignationModel> GetList();

        DesignationModel Get(string designationCode);

        DesignationModel Post(DesignationModel model);

        bool Update(DesignationModel designationDetails);

        bool Delete(int key);
    }
}
