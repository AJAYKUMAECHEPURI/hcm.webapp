﻿using HCM.Common.Models;
using HCM.WebApp.Module.Database;

namespace HCM.WebApp.WebApi.Services
{
    public interface IDepartmentServices
    {
        List<DepartmentModel> GetList();

        DepartmentModel Get(string departmentCode);

        DepartmentModel Post(DepartmentModel model);
        bool Update(DepartmentModel departmentDetails);

        bool Delete(int key);
    }
}
