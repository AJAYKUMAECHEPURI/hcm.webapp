﻿using DevExpress.ExpressApp.WebApi.Services;
using DevExpress.Xpo.DB;
using HCM.WebApp.Module.Database;
using HCM.WebApp.WebApi.Services;

namespace HCM.WebApp.WebApi.Helpers
{
    public static class Servicehelper
    {
        public static void AddServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddXpoDefaultUnitOfWork(true, options =>
            options.UseConnectionString(configuration.GetConnectionString("ConnectionString"))
        .UseAutoCreationOption(AutoCreateOption.DatabaseAndSchema)
        .UseEntityTypes(
                new Type[] {
                    typeof(Company),
                    typeof(BusinessUnit),
                    typeof(Department),
                    typeof(Designation),
                }
                ));
            services.ConfigureOptions<ConfigureJsonOptions>();
            services.AddScoped<ICompanyServices, CompanyServices>();
            services.AddScoped<IAdminServices, AdminServices>();
            services.AddScoped<ICompansationServices, CompansationServices>();
            services.AddScoped<IOnboardingServices, OnboardingServices>();
            services.AddScoped<IPayrollServices, PayrollServices>();
            services.AddScoped<IPerformanceServices, PerformanceServices>();
            services.AddScoped<IBUServices, BUServices>();
            services.AddScoped<IDesignationServices, DesignationServices>();
            services.AddScoped<IDepartmentServices, DepartmentServices>();
        }
    }
}

