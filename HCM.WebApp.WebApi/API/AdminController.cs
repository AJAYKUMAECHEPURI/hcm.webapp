﻿using HCM.WebApp.WebApi.Services;
using Microsoft.AspNetCore.Mvc;

namespace HCM.WebApp.WebApi.API
{
    [Route("api/Admin")]
    [ApiController]
    public class AdminController : BaseController
    {
        IAdminServices adminServices;
        public AdminController(IAdminServices adminServices) : base()
        {
            this.adminServices = adminServices;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return Ok();
        }
    }
}
