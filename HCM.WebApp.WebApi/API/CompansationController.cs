﻿using HCM.WebApp.WebApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HCM.WebApp.WebApi.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompansationController : BaseController
    {
        ICompansationServices compansationServices;
        public CompansationController(ICompansationServices compansationServices) : base()
        {
            this.compansationServices = compansationServices;
        }
    }
}
