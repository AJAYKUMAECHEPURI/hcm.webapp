﻿using DevExpress.Persistent.Base;
using HCM.Common.Models;
using HCM.WebApp.WebApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
namespace HCM.WebApp.WebApi.API
{
    [Route("api/Settings")]
    [ApiController]
    public class SettingsController : BaseController
    {
        readonly ICompanyServices companyServices;
        readonly IBUServices buServices;
        readonly IDepartmentServices departmentServices;
        readonly IDesignationServices designationServices;
        public SettingsController(ICompanyServices companyServices, IBUServices bUServices, IDepartmentServices departmentServices, IDesignationServices designationServices) : base()
        {
            this.companyServices = companyServices;
            this.buServices = bUServices;
            this.departmentServices = departmentServices;
            this.designationServices = designationServices;
        }

        #region CompanyServices

        [Route("GetProfileList")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetCompaniesList()
        {
            var result = companyServices.GetList();
            return Ok(result);
        }

        [Route("GetProfile")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetProfile([FromQuery] string companyCode)
        {
            var result = companyServices.Get(companyCode);
            return Ok(result);
        }

        [Route("UpdateProfile")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult UpdateProfile([FromBody] CompanyModel companyDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var result = companyServices.Update(companyDetails);
            return Ok(result);
        }



        [Route("PostProfile")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult PostProfile([FromBody] CompanyModel companyDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var result = companyServices.Post(companyDetails);
            return Ok(result);
        }

        [Route("DeleteProfile")]
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult DeleteProfile([FromQuery] int key)
        {
            if (key <= 0)
            {
                return BadRequest();
            }
            return Ok(companyServices.Delete(key));
        }

        #endregion CompanyServices

        #region BUServices

        [Route("GetBUList")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetBUList()
        {
            var result = buServices.GetList();
            return Ok(result);
        }

        [Route("GetBU")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetBU([FromQuery] string BUCode)
        {
            var result = buServices.Get(BUCode);
            return Ok(result);
        }

        [Route("AddBU")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult AddBU([FromBody] BUModel buDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var result = buServices.Post(buDetails);
            return Ok(result);
        }

        [Route("UpdateBU")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult UpdateBU([FromBody] BUModel buDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var result = buServices.Update(buDetails);
            return Ok(result);
        }


        [Route("DeleteBU")]
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult DeleteBU(int key)
        {
            if (key <= 0)
            {
                return BadRequest();
            }
            return Ok(buServices.Delete(key));
        }

        #endregion BUServices

        #region DepartmentServices

        [Route("GetDepartmentList")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetDepartmentList()
        {
            var result = departmentServices.GetList();
            return Ok(result);
        }

        [Route("GetDepartment")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetDepartment([FromQuery] string DepartmentCode)
        {
            var result = departmentServices.Get(DepartmentCode);
            return Ok(result);
        }

        [Route("AddDepartment")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult AddDepartment([FromBody] DepartmentModel deptDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var result = departmentServices.Post(deptDetails);
            return Ok(result);
        }

        [Route("UpdateDepartment")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult UpdateDepartment([FromBody] DepartmentModel departmentDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var result = departmentServices.Update(departmentDetails);
            return Ok(result);
        }


        [Route("DeleteDepartment")]
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult DeleteDepartment(int key)
        {
            if (key <= 0)
            {
                return BadRequest();
            }
            return Ok(departmentServices.Delete(key));
        }

        #endregion DepartmentServices


        #region Designation

        [Route("GetDesignationList")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetDesignationList()
        {
            var result = designationServices.GetList();
            return Ok(result);
        }

        [Route("GetDesignation")]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetDesignation([FromQuery] string designationCode)
        {
            var result = designationServices.Get(designationCode);
            return Ok(result);
        }

        [Route("AddDesignation")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult AddDesignation([FromBody] DesignationModel desigDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var result = designationServices.Post(desigDetails);
            return Ok(result);
        }

        [Route("UpdateDesignation")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult UpdateDesignation([FromBody] DesignationModel designationDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var result = designationServices.Update(designationDetails);
            return Ok(result);
        }

        [Route("DeleteDesignation")]
        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult DeleteDesignation(int key)
        {
            if (key <= 0)
            {
                return BadRequest();
            }
            return Ok(designationServices.Delete(key));
        }

        #endregion Designation
    }
}
