﻿using HCM.WebApp.WebApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HCM.WebApp.WebApi.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class OnboardingController : BaseController
    {
        IOnboardingServices onboardingServices;
        public OnboardingController(IOnboardingServices onboardingServices):base()
        {
            this.onboardingServices = onboardingServices;
        }
    }
}
