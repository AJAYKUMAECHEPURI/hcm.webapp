﻿using HCM.WebApp.WebApi.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HCM.WebApp.WebApi.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class PayrollController : BaseController
    {
        IPayrollServices payrollServices;
        public PayrollController(IPayrollServices payrollServices) : base()
        {
            this.payrollServices = payrollServices;
        }
    }
}
