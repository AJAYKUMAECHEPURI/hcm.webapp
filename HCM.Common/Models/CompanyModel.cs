﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCM.Common.Models
{
    public class CompanyModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Code { get; set; }

        [Required]
        public string Name { get; set; }

        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string SubDomain { get; set; }
        public string SiteURL { get; set; }
        public string CompanyLogo { get; set; }
        public int PrimaryCurrency { get; set; }
        public bool IsActive { get; set; }
    }
}
