﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCM.Common.Models
{
    public class DepartmentModel
    {
        public int Id { get; set; }
        public int BUId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public string Director { get; set; }
        public bool IsActive { get; set; }
    }
}
