﻿using Microsoft.AspNetCore.Mvc;

namespace HCM.WebApp.Blazor.Server.Controllers
{
    [Route("Dashboard")]
    public class DashboardController : Controller
    {
        [Route("Index")]
        public IActionResult Index()
        {
            ViewBag.title = "Index";
            return View();
        }
        public IActionResult Error() { 
        
        return View();
        }
    }
}
